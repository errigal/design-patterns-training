package com.errigal.training.design.patterns.session2.observer.spring;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import org.springframework.context.ApplicationEvent;

/**
 * Created by annadowling on 2019-04-10.
 *
 * Here we have a code example to demo observer design pattern in spring by using a custom application event.
 *
 * We create a simple CustomEvent class which extends ApplicationEvent
 */

public class CustomEvent extends ApplicationEvent {

    private String message;

    public CustomEvent(Object source, String message) {
        super(source);
        this.message = message;
    }
    public String getMessage() {
        return message;
    }
}
