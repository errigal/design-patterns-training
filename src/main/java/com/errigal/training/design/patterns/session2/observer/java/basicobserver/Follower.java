package com.errigal.training.design.patterns.session2.observer.java.basicobserver;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-23.
 * <p>
 * Follower implements the Observer interface therefore follower acts as an observer in this example
 */

class Follower implements Observer {
    private String followerName;

    public Follower(String followerName) {
        this.followerName = followerName;
    }

    /**
     * This method will be called to update all followers
     * regarding the new tweet posted by celebrity.
     *
     * @param celebrityName
     * @param tweet
     */
    @Override
    public void update(String celebrityName, String tweet) {
        System.out.println(followerName + " has received " + celebrityName + "'s tweet    :: " + tweet);
    }

    @Override
    public String toString() {
        return followerName;
    }
}
