package com.errigal.training.design.patterns.integration;
/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.ConnectException;
import java.util.List;

/**
 * User: milos.zubal
 * Date: 2019-05-13
 */
@Component
public class CamelExamples extends RouteBuilder {

  @Autowired
  ObjectMapper mapper;

  @Override
  public void configure() throws Exception {
    // @formatter:off

    rest("/hello").get()
      .route()
      .id("hello-route")
      .delay(1000)
      .setBody().constant("It works.")
      ;

    rest("/cbr-ftp").get("/{text}")
      .route()
      .id("cbr-ftp-reout")
      .setBody(constant("It works."))
      .setHeader("CamelFileName", header("text"))
      .choice()
        .when(header("text").contains("local"))
          .to("file:local")
        .otherwise()
          .to("ftp:milos@localhost?password=secret&passiveMode=true")
      ;

    from("file:input/splitter")
      .id("splitter-route")
      .convertBodyTo(String.class)
      .process(e -> {
        String body = e.getMessage().getBody(String.class);
        List deserialised = mapper.readValue(body, List.class);
        e.getMessage().setBody(deserialised);
      })
      .split().body()
      .setHeader("name", simple("${body[name]}"))
      .setBody(simple("${body[content]}"))
      .to("direct:output")
      .log("Finished processing of ${header.name}")
      ;

    from("direct:output")
      .id("output-route")
      .setHeader("CamelFileName", header("name"))
      .choice()
        .when(header("name").contains("local"))
          .to("file:local")
        .otherwise()
          .to("ftp:milos@localhost?password=secret&passiveMode=true")
      ;

    rest("/throttling").get()
      .route()
      .id("throttling-route")
      .throttle(1).rejectExecution(true)
      .delay(100)
      .setBody().constant("It works.")
      ;

    rest("/retry").get("/{text}")
      .route()
      .id("retry-route")
      .onException(ConnectException.class)
        .maximumRedeliveries(3)
        .redeliveryDelay(10000)
      .end()
      .setBody(constant("It works."))
      .setHeader("CamelFileName", header("text"))
      .to("ftp:milos@localhost?password=secret&passiveMode=true")
      ;

    from("twitter://search?type=direct&keywords=errigal&consumerKey=hCPheEMgU0Mw6T5uvmg8KD9gY&consumerSecret=MfB60wzhejBTb4qWP0u8FLJj0kVrAdwdVJpAuw0bC2I8mFUrX5&accessToken=2530106095-7Sp70N1qEdCNhUkKlgvwFhhY22GWjDP5iTMJLUS&accessTokenSecret=gl4lFYIFZnVGWVNGwttRjAyJieYG3paEgpKb9SMufB3Ty").autoStartup(true)
      .id("twitter-route")
      .autoStartup(true)
      .delayer(1000)
      .process(e -> {
        Object message = e.getMessage();
      })
      .log("${body}")
      ;
    // @formatter:on
  }
}
