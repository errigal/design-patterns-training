package com.errigal.training.design.patterns.session2.template.spring;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by annadowling on 2019-04-10.
 * <p>
 * Example of the template method abstract class in Spring.
 * GetToppings is the only Abstract method because is the only one that changes in every implementation.
 */

public abstract class PizzaPreparation {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * This is the only abstract method of this class. The subclass must provide an implementation to make a new preparation
     *
     * @return A list of toppings
     */
    public abstract List<String> getToppings();


    public Dough prepareDough() {
        logger.info("Preparing dough..");
        return new Dough();
    }

    public void rollOutDough(Dough dough) {
        logger.info("Rolling out dough..");
        dough.setRolledOut(true);
    }

    public Pizza addToppings(List<String> toppings, Dough dough) {
        logger.info("Adding Toppings..");
        return new Pizza(dough, toppings);

    }

    public final Pizza prepare() {
        Dough dough = prepareDough();
        rollOutDough(dough);
        Pizza pizza = addToppings(getToppings(), dough);
        logger.info("Your pizza is ready");
        return pizza;
    }

}
