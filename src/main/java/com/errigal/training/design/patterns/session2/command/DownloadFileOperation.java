package com.errigal.training.design.patterns.session2.command;

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-09.
 *
 * Download Implementation of FileOperation
 */

public class DownloadFileOperation implements FileOperation {

    private TextFile textFile;

    public DownloadFileOperation(TextFile textFile) {
        this.textFile = textFile;
    }

    @Override
    public String execute() {
        return textFile.downloadFile();
    }
}
