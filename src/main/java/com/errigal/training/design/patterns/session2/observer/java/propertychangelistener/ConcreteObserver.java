package com.errigal.training.design.patterns.session2.observer.java.propertychangelistener;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-23.
 *
 * Class to test out the PropertyChange Observer implementation by java.beans
 */

public class ConcreteObserver {

    public static void main(String[] args) {
        Subject s = new Subject();
        Observer o = new Observer();

        s.addObserver(o);
        s.setProperty("new");
    }

}

