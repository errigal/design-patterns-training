package com.errigal.training.design.patterns.session2.strategy;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-24.
 */

public class StrategyDemo {

    public static void main(String[] args) {

        // Creating Auth Context Object for logging in by
        // any auth strategy.
        AuthContext context = new AuthContext();

        // Setting BasicAuth strategy.
        context.setAuthStrategy(new BasicAuthStrategy());
        context.login("Anna");

        System.out.println("====================");

        // Setting OAuth strategy.
        context.setAuthStrategy(new OAuthStrategy());
        context.login("Anna");

        System.out.println("====================");
    }
}
