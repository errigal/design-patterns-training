package com.errigal.training.design.patterns.session2.template.java;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-24.
 */

abstract class Worker {

    /**
     * template method, final so subclasses can't override
     */
    public final void performDailyRoutine() {
        getUp();
        goToWork();
        relax();
        sleep();
    }

    /**
     * Common implementations of individual steps are defined in base class
     */
    private void getUp() {
        System.out.println("Good Morning!");
    }

    private void sleep() {
        System.out.println( "Good night!" );
    }

    /**
     * methods to be implemented by subclasses
     */
    abstract void goToWork();
    abstract void relax();

}

