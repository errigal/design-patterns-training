package com.errigal.training.design.patterns.session2.observer.java.basicobserver;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-23.
 *
 * The Subject interface handles adding, deleting and updating all observers
 */

interface Subject {
    public void register(Observer o);

    public void unregister(Observer o);

    public void notifyAllObservers(String s);
}
