package com.errigal.training.design.patterns.session2.observer.spring;
/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Created by annadowling on 2019-04-10.
 * <p>
 * Class demonstrates a Listener for the CustomEvent.
 * The only requirement for the listener is that it implements ApplicationListener
 *
 * This class is synchronous by default
 */

@Component
public class CustomEventListener implements ApplicationListener<CustomEvent> {

    @Override
    public void onApplicationEvent(CustomEvent event) {
        System.out.println("Received spring custom event - " + event.getMessage());
    }
}