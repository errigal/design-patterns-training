package com.errigal.training.design.patterns.session2.template.spring;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.util.List;

/**
 * Created by annadowling on 2019-04-10.
 */

public class Dough {
    private Boolean rolledOut;

    private List<String> toppings;

    public Boolean getRolledOut() {
        return rolledOut;
    }

    public List<String> getToppings() {
        return toppings;
    }

    public void setRolledOut(Boolean rolledOut) {
        this.rolledOut = rolledOut;
    }

    public void setToppings(List<String> toppings) {
        this.toppings = toppings;
    }
}
