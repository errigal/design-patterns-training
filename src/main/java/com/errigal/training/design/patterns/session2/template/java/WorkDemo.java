package com.errigal.training.design.patterns.session2.template.java;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-24.
 */

public class WorkDemo {
    public static void main(String[] args) {

        Worker worker = new FireMan();

        //using template method
        worker.performDailyRoutine();
        System.out.println("************");

        worker = new PostMan();

        worker.performDailyRoutine();
    }
}