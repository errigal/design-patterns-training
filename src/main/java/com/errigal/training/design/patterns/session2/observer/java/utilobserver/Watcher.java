package com.errigal.training.design.patterns.session2.observer.java.utilobserver;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.util.Observable;
import java.util.Observer;

/**
 * Created by annadowling on 2019-04-09.
 * <p>
 * This example is using java.util.Observer to demonstrate a more streamlined approach.
 * Our Observer interface is replaced with a class that implements Observer directly.
 * <p>
 * The java.util.Observer interface defines the update() method,
 * so there’s no need to define it ourselves as we did in the previous section.
 * <p>
 * <p>
 * DEPRECATED IN JAVA 9
 */

public class Watcher implements Observer {

    private String property;

    @Override
    public void update(Observable src, Object arg) {
        System.out.println("Variation of " + arg);
        System.out.println("Property in object " + src);
        this.setProperty((String) arg);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
