package com.errigal.training.design.patterns.session2.command;

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-09.
 *
 * FileOperation commands encapsulate all the information required for uploading and downloading a file,
 * including the receiver object, the methods to call,
 * and the arguments (in this case, no arguments are required, but there could be).
 *
 * Here we use the Java8 annotation @FunctionalInterface in order to flag this as
 * a Single Abstract Method (SAM) interface(a Java interface containing only one method.)
 */

@FunctionalInterface
public interface FileOperation {
    String execute();
}
