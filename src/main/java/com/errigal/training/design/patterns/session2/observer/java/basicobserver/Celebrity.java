package com.errigal.training.design.patterns.session2.observer.java.basicobserver;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.util.ArrayList;

/**
 * Created by annadowling on 2019-04-23.
 *
 * Celebrity implements the Subject Interface therefore Celebrity acts as a Subject in this example.
 */
class Celebrity implements Subject {

    private String celebrityName;
    private ArrayList<Observer> followers;

    public Celebrity(String celebrityName) {
        this.celebrityName = celebrityName;
        followers = new ArrayList<Observer>();
    }

    /**
     * add follower to the celebrity's registered follower list
     * @param o
     */
    @Override
    public void register(Observer o) {
        followers.add(o);
        System.out.println(o + " has started following " + celebrityName);
    }

    /**
     * remove follower from celebrity's registered follower list
     * @param o
     */
    @Override
    public void unregister(Observer o) {
        followers.remove(o);
        System.out.println(o + " has stopped following " + celebrityName);
    }

    /**
     * Notify all the registered followers
     * @param tweet
     */
    @Override
    public void notifyAllObservers(String tweet) {
        for (Observer follower : followers) {
            follower.update(celebrityName, tweet);
        }
        System.out.println();
    }

    /**
     * Method updates the tweet
     * It will internally call notifyAllObservers(tweet) method after updating the tweet
     * @param tweet
     */
    public void tweet(String tweet) {
        System.out.println("\n" + celebrityName + " has tweeted :: " + tweet + "\n");
        notifyAllObservers(tweet);
    }
}
