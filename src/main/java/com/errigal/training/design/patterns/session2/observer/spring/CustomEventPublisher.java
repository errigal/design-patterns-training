package com.errigal.training.design.patterns.session2.observer.spring;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Created by annadowling on 2019-04-10.
 * <p>
 * Class demonstrates a publisher of the CustomEvent.
 * The publisher constructs the event object and publishes it to anyone who’s listening.
 * <p>
 * To publish the event, the publisher can simply inject the ApplicationEventPublisher and use the publishEvent() API:
 */

public class CustomEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void doStuffAndPublishAnEvent(final String message) {
        System.out.println("Publishing custom event. ");
        CustomEvent customEvent = new CustomEvent(this, message);
        applicationEventPublisher.publishEvent(customEvent);
    }
}