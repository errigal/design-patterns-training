package com.errigal.training.design.patterns.session2.template.spring;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by annadowling on 2019-04-10.
 *
 * Example of Template method implementation in Spring
 * The main difference is the @Qualifier annotation. This annotation gives the opportunity to choose between implementations to use.
 */

@Component
@Qualifier("cheese")
public class CheesePizza extends PizzaPreparation{

    @Override
    public List<String> getToppings() {
        return Arrays.asList("mozzarella", "tomato", "basil","oil");
    }
}
