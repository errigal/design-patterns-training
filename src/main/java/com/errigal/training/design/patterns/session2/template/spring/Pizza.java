package com.errigal.training.design.patterns.session2.template.spring;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.util.List;

/**
 * Created by annadowling on 2019-04-10.
 */

public class Pizza {
    private Dough dough;

    private List<String> toppings;

    public Pizza(Dough dough, List<String> toppings) {
        super();
        this.dough = dough;
        this.toppings = toppings;
    }

    public List<String> getToppings() {
        return toppings;
    }

    public void setToppings(List<String> toppings) {
        this.toppings = toppings;
    }
}
