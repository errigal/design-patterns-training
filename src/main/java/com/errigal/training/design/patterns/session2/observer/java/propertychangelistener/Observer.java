package com.errigal.training.design.patterns.session2.observer.java.propertychangelistener;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by annadowling on 2019-04-23.
 */

public class Observer implements PropertyChangeListener {

    private String property;

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.println("Variation of " + evt.getPropertyName());
        System.out.println("\t(" + evt.getOldValue() +
                " -> " + evt.getNewValue() + ")");
        System.out.println("Property in object " + evt.getSource());
        this.setProperty((String) evt.getNewValue());
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

}
