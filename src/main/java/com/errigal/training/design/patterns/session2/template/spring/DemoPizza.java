package com.errigal.training.design.patterns.session2.template.spring;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;

/**
 * Created by annadowling on 2019-04-24.
 */

public class DemoPizza implements CommandLineRunner {
    @Autowired
    @Qualifier("cheese")
    Pizza pizza1;

    @Autowired
    @Qualifier("pepperoni")
    Pizza pizza2;


    @Override
    public void run(String... args) throws Exception {

        System.out.println(pizza1.getToppings());
        System.out.println(pizza2.getToppings());
    }
}

