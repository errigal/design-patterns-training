package com.errigal.training.design.patterns.session2.observer.java.propertychangelistener;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Created by annadowling on 2019-04-23.
 *
 * In this implementation, an Subject keeps a reference to the PropertyChangeSupport instance.
 * It sends the notifications to observers when a property of the class is changed.
 */

public class Subject {

    String property = "initial";

    // contains a support object instead of extending the support class
    PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public void addObserver(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener("theProperty", listener);
    }

    /**
     * Using this support, we can add and remove observers,
     * and notify them when the state of the observable changes:
     *
     * @param value
     */
    public void setProperty(String value) {
        String old = property;
        property = value;
        pcs.firePropertyChange("theProperty", old, value);
    }

    public String toString() {
        return "The subject object";
    }

}
