package com.errigal.training.design.patterns.session2.command;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-09.
 *
 * TextFile is the component that performs the file operations (Receiver).
 * This class performs a set of cohesive actions.
 * It’s the component that performs the actual action when the command’s execute() method is called.
 * Here we model TextFile for upload and download operations.
 */

public class TextFile {

    private String name;

    // constructor
    public TextFile(String name) {
        this.name = name;
    }

    public String uploadFile() {
        return "Uploading file " + name;
    }

    public String downloadFile() {
        return "Downloading file " + name;
    }

    // additional text file methods (editing, writing, copying, pasting)
}
