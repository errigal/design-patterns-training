package com.errigal.training.design.patterns.session2.command;

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.util.ArrayList;
import java.util.List;

/**
 * Created by annadowling on 2019-04-09.
 *
 * The class invoker(FileOperationExecutor) represents an object that knows how to execute a given command
 * but doesnt know how the command has been implemented. It only knows the command’s interface.
 */

public class FileOperationExecutor {

    private final List<FileOperation> fileOperations = new ArrayList<>();

    public String executeOperation(FileOperation fileOperation) {
        fileOperations.add(fileOperation);
        return fileOperation.execute();
    }
}
