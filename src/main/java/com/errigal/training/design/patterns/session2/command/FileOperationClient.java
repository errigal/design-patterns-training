package com.errigal.training.design.patterns.session2.command;

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-09.
 * <p>
 * <p>
 * The FileOperationClient represents an object that controls the command execution process
 * by specifying what commands to execute and at what stages of the process to execute them.
 * <p>
 * Here I am demonstrating three differing approaches to the Command Execution Process
 */

public class FileOperationClient {

    /**
     * @param args
     * In this first example we use the typical main method to invoke the file operations.
     */
    public static void main(String[] args) {
        FileOperationExecutor textFileOperationExecutor = new FileOperationExecutor();
        textFileOperationExecutor.executeOperation(new UploadFileOperation(new TextFile("file1.txt")));
        textFileOperationExecutor.executeOperation(new DownloadFileOperation(new TextFile("file1.txt")));
    }


    /**
     * The FileOperation Interface is a FunctionalInterface.
     * We can pass command objects using lambdas to the invoker, without creating the FileOperation instances explicitly.
     */
    private void lambdaExample() {
        FileOperationExecutor fileOperationExecutor = new FileOperationExecutor();
        fileOperationExecutor.executeOperation(() -> "Uploading file file1.txt");
        fileOperationExecutor.executeOperation(() -> "Downloading file file1.txt");
    }

    /**
     * We can also use method references for passing command objects to the invoker.
     */
    private void methodReferenceExample() {
        FileOperationExecutor fileOperationExecutor = new FileOperationExecutor();
        TextFile textFile = new TextFile("file1.txt");
        fileOperationExecutor.executeOperation(textFile::uploadFile);
        fileOperationExecutor.executeOperation(textFile::downloadFile);
    }
}

