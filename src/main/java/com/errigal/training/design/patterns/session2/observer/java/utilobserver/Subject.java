package com.errigal.training.design.patterns.session2.observer.java.utilobserver;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import java.util.Observable;

/**
 * Created by annadowling on 2019-04-09.
 *
 * Subject extends Observable and can therefore be thought of as our Subject.
 *
 * Note that we don’t need to call the observer’s update() method directly.
 * We just call setChanged() and notifyObservers(), and the Observable class is doing the rest for us.
 *
 * DEPRECATED IN JAVA 9
 */

public class Subject extends Observable {
    String property="initial";

    public void setProperty(String val) {
        property = val;
        setChanged();
        notifyObservers(val);
    }

    public String toString() { return "The subject object"; };
}
