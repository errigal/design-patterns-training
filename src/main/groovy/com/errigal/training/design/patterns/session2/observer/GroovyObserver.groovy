package com.errigal.training.design.patterns.session2.observer

import java.beans.PropertyChangeListener

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-10.
 *
 * Groovy comes with observable lists, maps and sets. Each of these collections trigger java.beans.PropertyChangeEvent
 * events when elements are added, removed or changed.
 * PropertyChangeEvent holds information on the property name and the old/new value a certain property has been changed to.
 */

class GroovyObserver {
  def event

  /**
   * Declares a PropertyChangeEventListener that is capturing the fired events
   * ObservableList.ElementEvent and its descendant types are relevant for this listener
   */
  def listener = {
    if (it instanceof ObservableList.ElementEvent) {
      event = it
    }
  } as PropertyChangeListener


  /**
   * Creates an ObservableList from the given list
   *  Triggers an ObservableList.ElementAddedEvent event
   */
  def observable = [1, 2, 3] as ObservableList

  /**
   * Validates the Registered listener
   */
  def validateObserverAdditon() {
    observable.addPropertyChangeListener(listener)
    observable.add 42

    assert event instanceof ObservableList.ElementAddedEvent

    def elementAddedEvent = event as ObservableList.ElementAddedEvent
    assert elementAddedEvent.changeType == ObservableList.ChangeType.ADDED
    assert elementAddedEvent.index == 3
    assert elementAddedEvent.oldValue == null
    assert elementAddedEvent.newValue == 42
  }

  /**
   * Validates the Registered listener clear method
   */
  def validateObserverClear() {
    observable.addPropertyChangeListener(listener)
    observable.clear()

    assert event instanceof ObservableList.ElementClearedEvent

    def elementClearedEvent = event as ObservableList.ElementClearedEvent
    assert elementClearedEvent.values == [1, 2, 3]
    assert observable.size() == 0
  }

}
