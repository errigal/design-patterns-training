package com.errigal.training.design.patterns.clean.code

import org.springframework.stereotype.Component


/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/

/**
 *
 * User: milos.zubal
 * Date: 2019-04-03
 */


// BAD
/**
 * Note for the training: the whole class would be much better implemented using builder pattern
 */
class CoffeeMachineBad {

  void makeCoffee(String result, Integer strength, Boolean hotWater, Boolean steamedMilk, Boolean frothedMilk, Boolean chocolatePowder, Boolean whiskey) {
    result = ""
    if (strength < 10) {
      if (strength < 1 && !hotWater && !steamedMilk && !frothedMilk && !chocolatePowder && whiskey) {
        result += "Ye just wanted a whiskey, huh?!"
      }
      result += "Strength: $strength + "
      if (hotWater) {
        result += "hot water + "
      } else {
        result += "no hot water + "
      }
      if (steamedMilk) {
        result += "steamed milk + "
      } else {
        result += "no steamed milk + "
      }
      if (frothedMilk) {
        result += "frothedMilk milk + "
      } else {
        result += "no frothedMilk milk + "
      }
      if (chocolatePowder) {
        result += "chocolatePowder + "
      } else {
        result += "no chocolatePowder + "
      }
      if (whiskey) {
        result += "whiskey + "
      } else {
        result += "no whiskey + "
      }
    } else {
      result = "We dont' make coffee that strong!"
    }
  }
}

@Component
class CoffeeMachineGood {

  public static final int STRENGTH_LIMIT = 10

  class CoffeeRequest {
    Integer strength
    Boolean hotWater
    Boolean steamedMilk
    Boolean frothedMilk
    Boolean chocolatePowder
    Boolean whiskey
  }

  // PUBLIC METHOD FIRST
  String makeCoffee(CoffeeRequest coffeeRequest) {
    if (tooStrong(coffeeRequest)) {
      return "We dont' make coffee that strong!"
    }
    if (justWhiskey(coffeeRequest)) {
      return "Ye just wanted a whiskey, huh?!"
    }
    String coffee = ""
    coffee += brewCoffee(coffeeRequest)
    coffee += addIngredients(coffeeRequest)
    return coffee
  }

  // NON-PUBLIC METHODS - ideally ordered by appearance
  boolean tooStrong(CoffeeRequest coffeeRequest) {
    coffeeRequest.strength < STRENGTH_LIMIT
  }

  boolean justWhiskey(CoffeeRequest coffeeRequest) {
    coffeeRequest.strength < 1 && !coffeeRequest.hotWater && !coffeeRequest.steamedMilk && !coffeeRequest.frothedMilk && !coffeeRequest.chocolatePowder && coffeeRequest.whiskey
  }

  String brewCoffee(CoffeeRequest coffeeRequest) {
    "Strength: $strength + "
  }

  String addIngredients(CoffeeRequest coffeeRequest) {
    String ingredients = ""
    ingredients += addWater(coffeeRequest)
    ingredients += addMilk(coffeeRequest)
    ingredients += addChocolate(coffeeRequest)
    ingredients += addWhiskey(coffeeRequest)
    ingredients
  }

  String addWater(CoffeeRequest coffeeRequest) {
    coffeeRequest.hotWater ? "hot water + " : "no hot water + "
  }

  void addMilk(CoffeeRequest coffeeRequest) {
    String milk = ""
    milk += coffeeRequest.steamedMilk ? "steamed milk + " : "no steamed milk + "
    milk += coffeeRequest.frothedMilk ? "frothed milk + " : "no frothed milk + "
    milk
  }

  String addChocolate(CoffeeRequest coffeeRequest) {
    coffeeRequest.chocolatePowder ? "chocolatePowder + " : "no chocolatePowder+ "
  }

  void addWhiskey(CoffeeRequest coffeeRequest) {
    coffeeRequest.whiskey ? "whiskey + " : "no whiskey + "
  }

}


