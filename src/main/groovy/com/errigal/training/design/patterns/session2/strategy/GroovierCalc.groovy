package com.errigal.training.design.patterns.session2.strategy

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-10.
 *
 * Here is the Groovier way to achieve the same thing as the Traditional Strategy pattern approach using Closures.
 */

class GroovierCalc {

  def calcStrategies = [
      { numberOne, numberTwo -> numberOne * numberTwo },
      { numberOne, numberTwo -> def result = 0; numberOne.times{ result += numberTwo }; result }
  ]

  def sampleData = [
      [3, 4, 12],
      [5, -5, -25]
  ]

  def validateCalculationData() {
    sampleData.each{ data ->
      calcStrategies.each { calc ->
        assert data[2] == calc(data[0], data[1])
      }
    }
  }
}
