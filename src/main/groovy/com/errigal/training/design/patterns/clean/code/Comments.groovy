package com.errigal.training.design.patterns.clean.code

import org.springframework.stereotype.Component


/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/

/**
 *
 * User: milos.zubal
 * Date: 2019-04-04
 */

// REDUNDANT COMMENTS
public class CoffeeRequest {
  Integer strength = 5 // this is strength of the coffee
  Boolean hotWater = false // add hot water?
  Boolean steamedMilk = false // add steamed milk?
  Boolean frothedMilk = false // add frothed milk ?
  Boolean chocolatePowder = false // add chocolatePowder?
  Boolean whiskey = false // add whiskey?
}

// NO CLASS LEVEL COMMENT
public class CoffeeMachineBadComments {

  // OUTDATED COMMENT - not changed after refactoring to coffee request, also just generated without any specific details
  /**
   * Makes coffee
   * @param result
   * @param strength
   * @param hotWater
   * @param steamedMilk
   * @param frothedMilk
   * @param chocolatePowder
   * @param whiskey
   */
  String makeCoffee(CoffeeRequest coffeeRequest) {

    // MUMBLING/RANTING - not helpful add
    // What the feck is going on here?
    Integer modifiedStrength = coffeeRequest.strength << 2

    // REDUNDANT COMMENT
    // Adding hot water
    if (hotWater) {
      result += "hot water + "
    } else {
      result += "no hot water + "
    }

    // REDUNDANT COMMENT
    // Adding steamed milk
    if (steamedMilk) {
      result += "steamed milk + "
    } else {
      result += "no steamed milk + "
    }
    // JOURNAL COMMENT + COMMENTED-OUT CODE
// Whiskey support removed in 3.6
//      if (whiskey) {
//        result += "whiskey + "
//      } else {
//        result += "no whiskey + "
//      }

  }

  // REDUNDANT COMMENT, also can become outdated/misleading if the implementation changes
  /*
    Brew the coffee using the strength
   */

  private String brewCoffee(CoffeeRequest coffeeRequest) {
    "Strength: $strength + "
  }


}

// DOCUMENTING INTENTION - class responsibility, links to other related classes, etc.
// xTODO Comment
/**
 * The main responsibility of this class is to make Coffee. Look also at #BeansProvider.
 * TODO: Refactor to Builder PatterIDMS-3209n
 */
@Component
public class CoffeeMachineGoodComments {

  public static final int STRENGTH_LIMIT = 10

  // INFORMATIVE COMMENT - documenting the public method intention, contract and error handling
  /**
   * Produces a coffee based on the passed #CoffeeRequest.
   * @param coffeeRequest
   * @return String describing coffee attributes delimited with '+' sign
   * @throws IllegalArgumentException in case of strength exceeding #STRENGTH_LIMIT
   */
  String makeCoffee(CoffeeRequest coffeeRequest) {

    // DOCUMENTING INTENTION - in this case a hacky optimisation (this will reduce at least one WTF during review :) )
    // utilising bit shift operator for faster multiplication
    Integer modifiedStrength = coffeeRequest.strength << 2
    if (tooStrong(coffeeRequest)) {
      throw new IllegalArgumentException("Too strong!")
    }
    if (justWhiskey(coffeeRequest)) {
      return "Ye just wanted a whiskey, huh?!"
    }
    String coffee = ""
    coffee += brewCoffee(coffeeRequest)
    coffee += addIngredients(coffeeRequest)
    return coffee
  }

  private boolean tooStrong(CoffeeRequest coffeeRequest) {
    coffeeRequest.strength > STRENGTH_LIMIT
  }

  private boolean justWhiskey(CoffeeRequest coffeeRequest) {
    coffeeRequest.strength < 1 && !coffeeRequest.hotWater && !coffeeRequest.steamedMilk && !coffeeRequest.frothedMilk && !coffeeRequest.chocolatePowder && coffeeRequest.whiskey
  }

  private String brewCoffee(CoffeeRequest coffeeRequest) {
    "Strength: $coffeeRequest.strength + "
  }

  private String addIngredients(CoffeeRequest coffeeRequest) {
    String ingredients = ""
    ingredients += addWater(coffeeRequest)
    ingredients += addMilk(coffeeRequest)
    ingredients += addChocolate(coffeeRequest)
    ingredients += addWhiskey(coffeeRequest)
    ingredients
  }

  private String addWater(CoffeeRequest coffeeRequest) {
    coffeeRequest.hotWater ? "hot water + " : "no hot water + "
  }

  private String addMilk(CoffeeRequest coffeeRequest) {
    String milk = ""
    milk += coffeeRequest.steamedMilk ? "steamed milk + " : "no steamed milk + "
    milk += coffeeRequest.frothedMilk ? "frothed milk + " : "no frothed milk + "
    milk
  }

  private String addChocolate(CoffeeRequest coffeeRequest) {
    coffeeRequest.chocolatePowder ? "chocolatePowder + " : "no chocolatePowder + "
  }

  private String addWhiskey(CoffeeRequest coffeeRequest) {
    coffeeRequest.whiskey ? "whiskey + " : "no whiskey + "
  }

}
