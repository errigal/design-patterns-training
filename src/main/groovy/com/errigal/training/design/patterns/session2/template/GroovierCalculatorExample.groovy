package com.errigal.training.design.patterns.session2.template

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-10.
 *
 * Here is the Groovier way to achieve the same thing as the Traditional Template Method pattern approach using Closures.
 *
 * Using closures this way looks more like the Strategy Pattern but if we realise that the built-in inject method is
 * the generic part of the algorithm for our template method,
 * The Closures become the customised parts of the template method pattern.
 */

class GroovierCalculatorExample {

/**
 * Here we are using Groovy’s inject method to achieve a similar result using Closures for the Addition
 */
  def printResultsAddition() {
    Closure addAll = { total, item -> total += item }
    def accumulated = [1, 2, 3, 4].inject(0, addAll)
    println accumulated    // => 10
  }

/**
 * Here we are using Groovy’s inject method to achieve a similar result using Closures for the Multiplication
 */
  def printResultsMultiply() {
    Closure multAll = { total, item -> total *= item }
    def accumulated = [1, 2, 3, 4].inject(1, multAll)
    println accumulated    // => 24
  }
}
