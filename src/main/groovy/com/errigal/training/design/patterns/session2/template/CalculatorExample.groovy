package com.errigal.training.design.patterns.session2.template

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-10.
 *
 * In this class we are invoking the traditional approach using polymorphism
 */

class CalculatorExample {
  static void main(String[] args) {
    println new Multiply().calculateTotal([1, 2, 3, 4])
    println new Addition().calculateTotal([1, 2, 3, 4])
  }

}
