package com.errigal.training.design.patterns.session2.strategy

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-10.
 *
 * In Groovy, because of its ability to treat code as a first class object using anonymous methods (which we loosely call Closures),
 * the need for the strategy pattern is greatly reduced. You can simply place algorithms inside Closures.
 *
 * Here we have a traditional Strategy pattern example in Groovy.
 */


/**
 * First we have defined an interface, we could also have used an abstract class.
 */
interface Calc {
  def executeCalculation(numberOne, numberTwo)
}


/**
 * Next we defined our algorithm strategies that implement Calc: CalcByMultiply
 */
class CalcByMultiply implements Calc {

  @Override
  def executeCalculation(numberOne, numberTwo) { numberOne * numberTwo }
}


/**
 * Next we defined out algorithm startegies that implements Calc: CalcByAdd
 */
class CalcByAdd implements Calc {

  @Override
  def executeCalculation(numberOne, numberTwo) {
    def result = 0
    numberOne.times {
      result += numberTwo
    }

    result
  }
}