package com.errigal.training.design.patterns.session2.strategy

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-10.
 *
 * In this class we are invoking the traditional approach using polymorphism
 */

class TestCalc {

  def sampleData = [
      [3, 4, 12],
      [5, -5, -25]
  ]

  Calc[] calcStrategies = [
      new CalcByMultiply(),
      new CalcByAdd()
  ]

  def validateCalculationData() {
    sampleData.each {
      data ->
        calcStrategies.each { calc ->
          assert data[2] == calc.executeCalculation(data[0], data[1])
        }
    }
  }

}
