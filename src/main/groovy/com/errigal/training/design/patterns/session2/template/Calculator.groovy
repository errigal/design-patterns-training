package com.errigal.training.design.patterns.session2.template

/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

/**
 * Created by annadowling on 2019-04-10.
 *
 * The Template Method Pattern abstracts away the details of several algorithms. The generic part of an algorithm is contained within a base class.
 * Particular implementation details are captured within base classes (Product and Sum).
 *
 * Here we have a traditional Template Method pattern example in Groovy.
 */

abstract class Calculator {
  protected initial

  abstract doCalculation(total, value)

  def calculateTotal(values) {
    def total = initial
    values.each { val -> total = doCalculation(total, val) }
    total
  }
}


/**
 * The base classes Product, provides a customised way to use the generic Calculator algorithm.
 */
class Multiply extends Calculator {

  def Multiply() { initial = 1 }

  @Override
  doCalculation(total, value) { total * value }
}

/**
 * The base classes Sum, provides a customised way to use the generic Calculator algorithm.
 */
class Addition extends Calculator {

  def Addition() { initial = 0 }

  @Override
  doCalculation(total, value) { total + value }
}
