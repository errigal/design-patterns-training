package com.errigal.training.design.patterns

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DesignPatternsTrainingApplication {

	static void main(String[] args) {
		SpringApplication.run(DesignPatternsTrainingApplication, args)
	}

}
