package com.errigal.training.design.patterns.clean.code


/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/

/**
 *
 * User: milos.zubal
 * Date: 2019-04-03
 */
class Naming {

  // SINGLE CHAR VARIABLES
  int d; // elapsed time in days

  int elapsedTimeInDays;


  // PRONOUNCEABLE
  class DtaRcrd102 {
    private Date genymdhms;
    private Date modymdhms;
  }

  class Customer {
    private Date generationTime
    private Date modificationTimestamp;
  }


  // SEARCHABLE
  static {
    int[] t = [1,2,3,4]
    int s = 0;
    for (int j = 0; j < 34; j++) {
      s += (t[j] * 4) / 5;
    }
  }

  static {
    int[] taskEstimates = [1,2,3,4]
    int numberOfTasks = taskEstimates.length
    int realDaysPerWeek = 4;
    final int WORK_DAYS_PER_WEEK = 5;
    int sum = 0;
    for (int i = 0; i < numberOfTasks; i++) {
      int realTaskDays = taskEstimates[i] * realDaysPerWeek;
      int realTaskWeeks = (realTaskDays / WORK_DAYS_PER_WEEK);
      sum += realTaskWeeks;
    }
  }


  // TECH DETAILS
  static {
    Map<String, Integer> workhoursPerDayList = [Mo: 8, Tu: 8, We: 8, Th: 8, Fr: 8, Sa: 0, Su: 0]
  }

  // GOOD
  static {
    Map<String, Integer> workhoursPerDay = [Mo: 8, Tu: 8, We: 8, Th: 8, Fr: 8, Sa: 0, Su: 0]
  }

}
