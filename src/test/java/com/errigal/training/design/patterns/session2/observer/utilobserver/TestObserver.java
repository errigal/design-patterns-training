package com.errigal.training.design.patterns.session2.observer.utilobserver;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import com.errigal.training.design.patterns.session2.observer.java.utilobserver.Subject;
import com.errigal.training.design.patterns.session2.observer.java.utilobserver.Watcher;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by annadowling on 2019-04-14.
 * <p>
 * Observable contains a list of observers and exposes methods to maintain that list
 * – addObserver() and deleteObserver().
 * <p>
 * In order to test the result, we just need to add the observer to this list and to set the news:
 */

public class TestObserver {

    Subject observable = new Subject();
    Watcher observer = new Watcher();

    @Test
    public void testObserverUpdate() {
        observable.addObserver(observer);
        observable.setProperty("testProp");

        assertEquals(observer.getProperty(), "testProp");
    }
}
