package com.errigal.training.design.patterns.session2.observer.propertychangelistener;/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 *************************************************************** */

import com.errigal.training.design.patterns.session2.observer.java.propertychangelistener.Observer;
import com.errigal.training.design.patterns.session2.observer.java.propertychangelistener.Subject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by annadowling on 2019-04-14.
 *
 * Due to the PropertyChangeSupport class which is doing the wiring for us,
 * we can restore the new property value from the event.
 *
 * We can Test this in the following testObserverUpdate() method.
 */

public class TestObserver {
    Subject observable = new Subject();
    Observer observer = new Observer();

    @Test
    public void testObserverUpdate() {
        observable.addObserver(observer);
        observable.setProperty("testProp");

        assertEquals(observer.getProperty(), "testProp");
    }

}
