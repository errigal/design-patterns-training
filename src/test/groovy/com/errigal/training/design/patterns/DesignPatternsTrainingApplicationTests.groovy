package com.errigal.training.design.patterns

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class DesignPatternsTrainingApplicationTests {

	@Test
	void contextLoads() {
	}

}
