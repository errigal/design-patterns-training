package com.errigal.training.design.patterns.clean.code

import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class CoffeeMachineTests {

	@Autowired
	CoffeeMachineGoodComments coffeeMachineGood

	@Test
	void "coffee only - happy day"() {
		String result = coffeeMachineGood.makeCoffee(new CoffeeRequest(strength: 7))
		Assert.assertThat(result, CoreMatchers.is("Strength: 7 + no hot water + no steamed milk + no frothed milk + no chocolatePowder + no whiskey + "))
	}

	@Test
	void "all ingredients - happy day"() {
		String result = coffeeMachineGood.makeCoffee(new CoffeeRequest(strength: 7, hotWater: true, steamedMilk: true, frothedMilk: true, chocolatePowder: true, whiskey: true))
		Assert.assertThat(result, CoreMatchers.is("Strength: 7 + hot water + steamed milk + frothed milk + chocolatePowder + whiskey + "))
	}

	@Test
	void "whiskey only - happy day"() {
		String result = coffeeMachineGood.makeCoffee(new CoffeeRequest(strength: 0, whiskey: true))
		Assert.assertThat(result, CoreMatchers.is("Ye just wanted a whiskey, huh?!"))
	}

	@Test(expected = IllegalArgumentException)
	void "too strong - bad day"() {
		String result = coffeeMachineGood.makeCoffee(new CoffeeRequest(strength: 20))
	}

}
